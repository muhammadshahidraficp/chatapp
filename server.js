
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

var http = require('http').Server(app);
var io = require('socket.io')(http);


var mongoose = require('mongoose');

dbUrl = 'mongodb://adminuser:adminuser123@ds263048.mlab.com:63048/chatappindia';

app.use(express.static(__dirname));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}))

var port = process.env.PORT || 4000;

var Message = mongoose.model('Message', {
	name: String,
	message: String
});

// var messages = [
//  	{ name:"Shahid",
//  	  message:"Hello saif"
//  	},
//  	{ 
//  	  name:"Saif", 
//  	  message: "Hi Shahid"
//  	}
// ];

app.get('/message',(req,res) => {
	var finalData = [];
	Message.find({}, (err,messages) => {
		messages.forEach(function(item){
			if(typeof item.name === 'undefined') {
			  	
			} else {
				finalData.push(item);
			}
			//console.log(item.name)
  			//copy.push(item*item);
		});
		res.send(finalData);
	});
});

app.post('/message',(req,res) => {
	var message = new Message(req.body);
	message.save((err) => {
		if(err) {
			res.sendStatus(500);
		}else {
			//messages.push(req.body);
			io.emit('message',req.body);
	 		res.sendStatus(201);
		}
	});
})

io.on('connection',(socket) =>{
	console.log('user connected');
})

mongoose.connect(dbUrl,{ useNewUrlParser: true, useUnifiedTopology: true } , (err) => {
	console.log('mlab connected');
});

http.listen(port,() => {
	console.log(`server running on ${port}`);
});





